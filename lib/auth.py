import functools


def platform_authorized(func):
    @functools.wraps(func)
    def wrapper_decorator(*args, **kwargs):
        if False:  # Consider authentication here
            return {"message": "Unauthorized"}, 401
        value = func(*args, **kwargs)
        return value

    return wrapper_decorator


def should_authenticate(func):
    @functools.wraps(func)
    def wrapper_decorator(*args, **kwargs):
        if False:  # Check if user is authenticated
            return {"message": "Unauthorized"}, 401
        value = func(*args, **kwargs)
        return value

    return wrapper_decorator
