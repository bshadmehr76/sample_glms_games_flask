from flask_restx import Resource
from glms_games.extentions import api
from glms_games.blueprints.games.dto import (
    MultipleQuestionGameDTO,
)
from flask_restx import marshal
from bson import ObjectId
from glms_games.blueprints.games.models.multiple_choice import MultipleQuestionGame as MultipleQuestionGameModel

games = api.namespace("games", description="games API")


@games.param("game_id", "The game identifier")
@games.response(400, "Bad Request")
@games.response(200, "Success")
class BaseGame(Resource):
    def __init__(self, *args, **kwargs):
        super(BaseGame, self).__init__(*args, **kwargs)

    def get(self, game_id):
        game = self.MODEL.objects.get(id=ObjectId(game_id))
        return marshal(game.to_mongo(), self.SERIALIZER), 200
