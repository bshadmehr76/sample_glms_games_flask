from glms_games.extentions import db
from glms_games.blueprints.models import BaseGame


class MultipleChoiceOption(db.EmbeddedDocument):
    id = db.StringField(required=True)
    title = db.StringField(required=True)
    is_correct = db.IntField(required=True)


class MultipleQuestionGame(BaseGame):
    level_id = db.StringField(required=True)
    course_id = db.StringField(required=True)
    subject_id = db.StringField(required=True)
    answers = db.EmbeddedDocumentListField(MultipleChoiceOption)
