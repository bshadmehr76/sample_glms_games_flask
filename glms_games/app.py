from flask import Flask
from glms_games.extentions import db, api

from glms_games.blueprints.games import games  # noqa
import os


def create_app(settings_override=None):
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object("config.settings")
    my_instance_location = os.path.join(os.getcwd(), "instance")
    app.config.from_pyfile(f"{my_instance_location}/settings.py", silent=False)

    if settings_override is not None:
        app.config.update(settings_override)

    extentions(app)

    print(app.url_map)

    return app


def extentions(app):
    db.init_app(app)
    api.init_app(app)
    return None
