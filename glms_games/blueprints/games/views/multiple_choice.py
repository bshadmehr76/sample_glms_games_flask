from flask_restx import Resource
from glms_games.extentions import api
from glms_games.blueprints.games.dto import (
    MultipleQuestionGameDTO,
)
from glms_games.blueprints.games.models.multiple_choice import MultipleQuestionGame as MultipleQuestionGameModel
from glms_games.blueprints.games.models.multiple_choice import MultipleChoiceOption as MultipleChoiceOptionModel
from glms_games.blueprints.games.views.base import BaseGame
from glms_games.blueprints.games.enums import GameType
from faker import Faker
import uuid
from random import randint

games = api.namespace("multiple_choice", description="games API")


@games.route("/<string:game_id>")
class MultipleQuestionGame(BaseGame):
    def __init__(self, *args, **kwargs):
        self.MODEL = MultipleQuestionGameModel
        self.SERIALIZER = MultipleQuestionGameDTO
        super(MultipleQuestionGame, self).__init__(*args, **kwargs)


@games.route("/multiple_choice")
class MultipleQuestionGameMock(Resource):
    def post(self):
        fake = Faker()
        for i in range(10):
            game = MultipleQuestionGameModel()
            game.type = GameType.MultipleChoiceQuestion
            game.title = fake.name()
            game.description = fake.text()
            game.level_id = str(uuid.uuid4())
            game.created_by = str(uuid.uuid4())
            game.course_id = str(uuid.uuid4())
            game.subject_id = str(uuid.uuid4())
            answers = []
            for j in range(randint(3, 6)):
                answers.append(
                    MultipleChoiceOptionModel(
                        id=str(uuid.uuid4()),
                        title=fake.name(),
                        is_correct=randint(0,100) > 50
                    )
                )
            game.answers = answers
            game.save()
        return {}
