from glms_games.extentions import db
import datetime
from glms_games.blueprints.games.enums import GamePlayedStatus, GameType


class BaseGame(db.Document):
    meta = {'allow_inheritance': True, 'collection': 'games'}
    created_at = db.DateTimeField(default=datetime.datetime.utcnow)
    updated_at = db.DateTimeField(default=datetime.datetime.utcnow)
    created_by = db.StringField(required=True)
    title = db.StringField(required=True)
    description = db.StringField(required=True)
    type = db.EnumField(GameType, required=True)


class GamePlayed(db.Document):
    created_at = db.DateTimeField(default=datetime.datetime.utcnow)
    updated_at = db.DateTimeField(default=datetime.datetime.utcnow)
    player = db.StringField(required=True)
    game_id = db.StringField(required=True)
    status = db.EnumField(GamePlayedStatus)
    state = db.DictField()
