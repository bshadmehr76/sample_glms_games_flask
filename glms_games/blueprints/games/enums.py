from enum import Enum


class GamePlayedStatus(Enum):
    Started = 1
    Replaying = 2
    Succeeded = 3
    Failed = 4


class GameType(Enum):
    MultipleChoiceQuestion = "multiple_choice_question"
