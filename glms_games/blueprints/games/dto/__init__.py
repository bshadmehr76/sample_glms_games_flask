from glms_games.extentions import api
from flask_restx import fields
from glms_games.blueprints.dto import GeneralDTO

GeneralGameDTO = api.inherit(
    "GeneralGameDTO",
    GeneralDTO,
    {
        "created_at": fields.DateTime(required=False, description="Created at"),
        "updated_at": fields.DateTime(required=False, description="Updated_at at"),
        "created_by": fields.String(required=True, description="Created by user id"),
        "title": fields.String(required=True, description="title"),
        "description": fields.String(required=True, description="description"),
        "type": fields.String(required=False, description="type"),
    },
)

MultipleChoiceOptionDTO = api.model(
    "MultipleChoiceOptionDTO",
    {
        "id": fields.String(required=True, description="level_id"),
        "title": fields.String(required=True, description="title"),
        "is_correct": fields.Boolean(required=True, description="is_correct"),
    },
)


MultipleQuestionGameDTO = api.inherit(
    "MultipleQuestionGameDTO",
    GeneralGameDTO,
    {
        "level_id": fields.String(required=True, description="level_id"),
        "course_id": fields.String(required=True, description="level_id"),
        "subject_id": fields.String(required=True, description="level_id"),
        "answers": fields.List(fields.Nested(MultipleChoiceOptionDTO), required=True, description="answers"),
    },
)
