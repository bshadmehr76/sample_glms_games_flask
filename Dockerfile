FROM python:latest

ENV INSTALL_PATH /glms_games

RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

RUN apt-get update
RUN apt-get -y install python3-pip python3-dev libpq-dev postgresql postgresql-contrib

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt
COPY config .
CMD gunicorn -b 0.0.0.0:8888 --access-logfile - "glms_games.app:create_app()"
